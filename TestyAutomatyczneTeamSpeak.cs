using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;

namespace TeamSpeak
{
    [TestClass]
    public class BasicScenarios
    {
        protected const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";
        protected static RemoteWebDriver TeamspeakSession;

        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            Process.Start(@"C:\Program Files (x86)\Windows Application Driver\WinAppDriver.exe");

            var appiumOptions = new AppiumOptions();
            appiumOptions.AddAdditionalCapability("deviceName", "WindowsPC");
            appiumOptions.AddAdditionalCapability("app", "C:\\Program Files\\TeamSpeak 3 Client\\ts3client_win64.exe");

            TeamspeakSession = new RemoteWebDriver(new Uri("http://127.0.0.1:4723"), appiumOptions);
            Assert.IsNotNull(TeamspeakSession);


            TeamspeakSession.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);


        }

        [TestMethod]
        public void DodanieTozsamosci()
        {
            TeamspeakSession.FindElementByName("Narzędzia").Click();
            TeamspeakSession.FindElementByName("Tożsamości").Click();

            var element1 = TeamspeakSession.FindElementsByClassName("QLineEdit");

            element1[0].Click();
            element1[0].SendKeys(Keys.LeftControl + "A");
            element1[0].SendKeys("User1");


            element1[1].Click();
            element1[1].SendKeys(Keys.LeftControl + "A");
            element1[1].SendKeys("UserNick1");

            TeamspeakSession.FindElementByName("Utwórz").Click();
            var element2 = TeamspeakSession.FindElementByName("User1");
            String x = element2.GetAttribute("Name");
            Assert.AreEqual("User1", x);

            element2.Click();
            System.Threading.Thread.Sleep(100);
            TeamspeakSession.FindElementByName("Usuń").Click();
            System.Threading.Thread.Sleep(100);
            TeamspeakSession.FindElementByName("Tak").Click();
            System.Threading.Thread.Sleep(100);
            TeamspeakSession.FindElementByName("Anuluj").Click();
            TeamspeakSession.FindElementByName("Odrzuć").Click();
        }



        [TestMethod]
        public void Pomoc()
        {
            System.Threading.Thread.Sleep(500);

            TeamspeakSession.FindElementByName("Pomoc").Click();
            TeamspeakSession.FindElementByName("Informacje o TeamSpeak 3").Click();

            String x = TeamspeakSession.FindElementByName("3.5.6  (25.11.2020 14:53:42)").GetAttribute("Name");
            Assert.AreEqual("3.5.6  (25.11.2020 14:53:42)", x);

            TeamspeakSession.FindElementByClassName("QPushButton").Click();
            System.Threading.Thread.Sleep(500);

        }


        [TestMethod]
        public void Serwer()
        {

            TeamspeakSession.FindElementByName("Połączenia").Click();
            TeamspeakSession.FindElementByName("Połącz").Click();


            var element1 = TeamspeakSession.FindElementsByClassName("QLineEdit");

            element1[0].Click();
            element1[0].SendKeys(Keys.LeftControl + "A");
            element1[0].SendKeys("User");


            element1[1].Click();
            element1[1].Clear();
            element1[1].SendKeys(Keys.LeftControl + "A");
            element1[1].SendKeys("ts.net.pl");
            element1[1].SendKeys(Keys.Enter);


            System.Threading.Thread.Sleep(2000);
            var element2 = TeamspeakSession.FindElementsByClassName("QLabel");
            String x = element2[1].GetAttribute("Name");
            Assert.AreEqual("Połączono jako User", x);
            System.Threading.Thread.Sleep(400);

            TeamspeakSession.FindElementByName("Zamknij").Click();

        }
        [TestMethod]
        public void Intefrejs()
        {
            
            var element1 = TeamspeakSession.FindElementByClassName("QMenuBar");
            var element2 = TeamspeakSession.FindElementByClassName("QSplitter");
            var element3 = TeamspeakSession.FindElementByClassName("ImprovedToolBar");
            var element4 = TeamspeakSession.FindElementByClassName("QStatusBar");            

            bool wyswietlone = element1.Displayed & element2.Displayed & element3.Displayed & element4.Displayed;
            Assert.IsTrue(wyswietlone);
           
        }
    }
}